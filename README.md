# Set your token as environment variable

export MCULE_API_TOKEN=<your_token>

# Send quote request

```
cat test_mcule_ids.txt | python send_quote_request.py --delivery-country=US --amount=1 --extra-data='{"purity": 90}'
```

Or you can specify the token with the `--token` option:

```
cat test_mcule_ids.txt | python send_quote_request.py --token=<your_token> --delivery-country=US --amount=1
```

# Get quotes

```
python get_quotes.py --quote-request-id=<quote_request_id>
```

Or to save the quote files (PDF, xlsx, json):

```
python get_quotes.py --quote-request-id=<quote_request_id> --save-files
```

# Order quote

With a PO file:

```
python order_quote.py --quote-id=<quote_id> --po-file=<path_to_PO_file> --extra-data='{"po_number": "PO_123456", "shipping_country": "US", "shipping_first_name": "John", "shipping_last_name": "Doe", "notes": "My notes"}'
```

Without a PO file:

```
python order_quote.py --quote-id=<quote_id> --extra-data='{"shipping_country": "US", "shipping_first_name": "John", "shipping_last_name": "Doe", "notes": "My notes"}'
```
