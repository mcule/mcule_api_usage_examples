import os
import argparse
import logging
import requests
import sys
import json
from urllib.parse import urljoin


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


API_BASE_URL_DEFAULT = 'https://mcule.com/api/v1/'
QUOTE_REQUEST_API_URL = 'iquote-queries/'


def run(token, delivery_country, amount, api_base_url=API_BASE_URL_DEFAULT, extra_data=None):
    mcule_ids = [line.strip() for line in sys.stdin]
    logger.debug('number of loaded mcule IDs: %s', len(mcule_ids))

    mcule_ids_set = set(mcule_ids)
    logger.debug(
        'number of mcule IDs after removing duplicates: %s',
        len(mcule_ids_set)
    )

    post_data = {
        'amount': amount or None,
        'delivery_country': delivery_country,
        'mcule_ids': list(mcule_ids_set),
    }
    if extra_data:
        post_data.update(extra_data)

    headers = {
        'Authorization': 'Token {}'.format(token),
        'Content-Type': 'application/json',
    }
    url = urljoin(api_base_url, QUOTE_REQUEST_API_URL)
    logger.info('URL: %s', url)

    response = requests.post(url, data=json.dumps(post_data), headers=headers)
    if response.ok:
        qr = response.json()
        logger.info('Succcessful quote request. Response: %s', qr)
        logger.info('Quote request ID: %s', qr['id'])
        logger.info('Quote request API URL: %s', qr['api_url'])
        sys.stdout.write(json.dumps(qr, indent=2))
    else:
        logger.info(
            'An error occured during quote request call: code: %s, content: %s',
            response.status_code, response.content
        )
        try:
            qr = response.json()
            sys.stdout.write(json.dumps(qr, indent=2))
        except Exception as e:
            pass


if __name__ == "__main__":
    description = """
Sends a quote request.

recommended usage:
cat test_mcule_ids.txt | python send_quote_request.py --token=<your_token> --delivery-country=US --amount=1
"""
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        '--token',
        type=str,
        default=os.environ.get('MCULE_API_TOKEN'),
    )
    parser.add_argument(
        '--api-base-url',
        type=str,
        default=os.environ.get('MCULE_API_BASE_URL', API_BASE_URL_DEFAULT),
    )
    parser.add_argument(
        '--amount', type=str, default=1, help="target amount in mg"
    )
    parser.add_argument(
        '--delivery-country',
        type=str,
        default='US',
        help="ISO 3166-1 alpha-2 code of the delivery country."
    )
    parser.add_argument(
        '--extra-data',
        type=str,
        help="extra POST data in json format"
    )

    args = parser.parse_args()

    extra_data = args.extra_data
    if extra_data:
        extra_data = json.loads(extra_data)

    run(
        token=args.token,
        api_base_url=args.api_base_url,
        amount=args.amount,
        delivery_country=args.delivery_country,
        extra_data=extra_data,
    )
