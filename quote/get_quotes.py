import os
import argparse
import logging
import requests
import sys
import json
import time
import re
from urllib.parse import urljoin


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


API_BASE_URL_DEFAULT = 'https://mcule.com/api/v1/'
QUOTE_REQUEST_DETAIL_API_URL = 'iquote-queries/{quote_request_id}/'
QUOTE_DETAIL_API_URL = 'iquotes/{quote_id}/'

QUOTE_REQUEST_STATE_DONE = 30
QUOTE_REQUEST_STATE_ERROR = 40


def get_quotes(token, api_base_url, qr):
    group = qr['group']
    if not group:
        return []

    for quote in group.get('quotes', []):
        quote_id = quote['id']
        headers = {
            'Authorization': 'Token {}'.format(token),
            'Content-Type': 'application/json',
        }
        url = urljoin(
            api_base_url,
            QUOTE_DETAIL_API_URL.format(quote_id=quote_id)
        )
        logger.info('URL: %s', url)
        response = requests.get(url, headers=headers)
        quote_data = response.json()
        yield quote_data


def run(token, quote_request_id, api_base_url=API_BASE_URL_DEFAULT, check_freq=60, save_files=False):
    headers = {
        'Authorization': 'Token {}'.format(token),
        'Content-Type': 'application/json',
    }
    url = urljoin(
        api_base_url,
        QUOTE_REQUEST_DETAIL_API_URL.format(quote_request_id=quote_request_id)
    )
    logger.info('URL: %s', url)

    def check_status():
        response = requests.get(url, headers=headers)
        if response.ok:
            qr = response.json()
            logger.info('Succcessful API request. Response: %s', qr)
            logger.info('Quote request ID: %s', qr['id'])
            logger.info('Quote request API URL: %s', qr['api_url'])
            logger.info('Quote request status: %s', qr['state_display'])

            status = qr['state']
            if status in [QUOTE_REQUEST_STATE_DONE, QUOTE_REQUEST_STATE_ERROR]:
                if status == QUOTE_REQUEST_STATE_ERROR:
                    logger.info(
                        'An error happened during the '
                        'processing of the quote request.'
                    )
                    return None
                if status == QUOTE_REQUEST_STATE_DONE:
                    if not qr['group']:
                        logger.info('Quotes can not be generated for the query.')
                        return None
                    else:
                        return qr
            return 1
        else:
            logger.info(
                'An error occured during the request: code: %s, content: %s',
                response.status_code, response.content
            )
            return None

    while True:
        ret = check_status()
        if ret is None:
            return
        if ret == 1:
            logger.info('Check state again in %s seconds.', check_freq)
            time.sleep(check_freq)
        else:
            logger.info('Quote request processing finished: %s', ret)
            sys.stdout.write(json.dumps(ret, indent=2))
            if save_files:
                with open('quote_request_{}.json'.format(ret['id']), 'w') as f:
                    f.write(json.dumps(ret, indent=2))
            for q in get_quotes(token, api_base_url, ret):
                sys.stdout.write(json.dumps(q, indent=2))
                if save_files:
                    with open('{}.json'.format(q['reference_id_full']), 'w') as f:
                        f.write(json.dumps(q, indent=2))

                    # download PDF and excel file
                    for route in ('download-pdf/', 'download-excel/'):
                        download_url = urljoin(
                            api_base_url,
                            QUOTE_DETAIL_API_URL.format(quote_id=q['id']),
                        ) + route
                        logger.info('Download URL: %s', download_url)
                        resp = requests.get(download_url, headers=headers)
                        if resp.ok:
                            cd = resp.headers['content-disposition']
                            fname = re.findall("filename=(.+)", cd)[0]
                            with open(fname, 'wb') as f:
                                f.write(resp.content)
            break


if __name__ == "__main__":
    description = """
Get quotes for a quote request.

recommended usage:
python get_quotes.py --token=<your_token> --quote-request-id=<quote_request_id>
"""
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        '--token',
        type=str,
        default=os.environ.get('MCULE_API_TOKEN'),
    )
    parser.add_argument(
        '--api-base-url',
        type=str,
        default=os.environ.get('MCULE_API_BASE_URL', API_BASE_URL_DEFAULT),
    )
    parser.add_argument(
        '--quote-request-id',
        type=str,
    )
    parser.add_argument(
        '--save-files',
        action='store_true',
        help=(
            'Save quote request response json, quotes response json, '
            'quote PDF / excel files to files'
        )
    )
    args = parser.parse_args()

    run(
        token=args.token,
        api_base_url=args.api_base_url,
        quote_request_id=args.quote_request_id,
        save_files=args.save_files,
    )
