import os
import argparse
import logging
import requests
import sys
import json
from urllib.parse import urljoin


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


API_BASE_URL_DEFAULT = 'https://mcule.com/api/v1/'
ORDER_QUOTE_API_URL = 'iquotes/{quote_id}/order/'

PO_OPTION_UPLOAD_PO = 10
PO_OPTION_ORDER_WITHOUT_PO = 20


def run(quote_id, token, api_base_url=API_BASE_URL_DEFAULT, po_file=None, extra_data=None):
    headers = {
        'Authorization': 'Token {}'.format(token),
    }
    if po_file:
        po_option = PO_OPTION_UPLOAD_PO
        post_files = {'po_file': open(po_file, 'rb')}
    else:
        po_option = PO_OPTION_ORDER_WITHOUT_PO
        post_files = None
    post_data = {
        'po_option': po_option
    }
    if extra_data:
        post_data.update(extra_data)

    url = urljoin(api_base_url, ORDER_QUOTE_API_URL.format(quote_id=quote_id))
    logger.info('URL: %s', url)
    logger.info('files: %s | data: %s', post_files, post_data)

    response = requests.post(url, files=post_files, data=post_data, headers=headers)
    if response.ok:
        qr = response.json()
        logger.info('Succcessful quote order. Response: %s', qr)
        sys.stdout.write(json.dumps(qr, indent=2))
    else:
        logger.info(
            'An error occured during quote order call: code: %s, content: %s',
            response.status_code, response.content
        )
        try:
            qr = response.json()
            sys.stdout.write(json.dumps(qr, indent=2))
        except Exception as e:
            pass


if __name__ == "__main__":
    description = """
Order a quote.

recommended usage:
python order_quote.py --token=<your_token> --quote-id=<quote_id> --po-file=<po_file> --extra-data='{"shipping_country": "US", "shipping_first_name": "John", "shipping_last_name": "Doe"}'
"""
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument(
        '--token', type=str,
        default=os.environ.get('MCULE_API_TOKEN'),
    )
    parser.add_argument(
        '--api-base-url',
        type=str,
        default=os.environ.get('MCULE_API_BASE_URL', API_BASE_URL_DEFAULT),
    )
    parser.add_argument(
        '--quote-id', type=str,
    )
    parser.add_argument(
        '--po-file', type=str,
    )
    parser.add_argument(
        '--extra-data',
        type=str,
        help="extra POST data in json format"
    )

    args = parser.parse_args()

    extra_data = args.extra_data
    if extra_data:
        extra_data = json.loads(extra_data)

    run(
        token=args.token,
        api_base_url=args.api_base_url,
        quote_id=args.quote_id,
        po_file=args.po_file,
        extra_data=extra_data,
    )
